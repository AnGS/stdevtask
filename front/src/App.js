
import {Route, Switch} from 'react-router-dom'
import { BrowserRouter as Router } from 'react-router-dom';
import GameSelector from './components/GameSelector'
import RandomQuestions from './components/RandomQuestions'


function App() {
  return (
    <Router>
        <Switch>
          <Route path="/" component={GameSelector} exact />
          <Route path="/r/:topic" component={RandomQuestions} exact />
        </Switch>
    </Router>
  );
}

export default App

import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import ConnectApi from "../ConnectApi";
import Button from "@material-ui/core/Button";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Link from "@material-ui/core/Link";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import { Alert, AlertTitle } from "@material-ui/lab";

export const RandomQuestions = () => {
  const { topic } = useParams();
  const API_URL = "http://127.0.0.1:8000/quiz/r/" + topic;
  const [dataState] = ConnectApi(API_URL);
  const answers = dataState.data.flatMap((question) => question.answer);
  const countOfAnswers = answers.length;
  const [answer, setAnswer] = useState({});
  const [answerCheck, setAnswerCheck] = useState();

  useEffect(() => {
    if (Object.keys(answer).length === 0) {
      setAnswer(createInitalAnswers());
    }
  }, [answer]);

  const handleSelection = (evt) => {
    setAnswer({ ...answer, [evt.target.value]: evt.target.checked });
  };

  const createInitalAnswers = () => {
    let objectId = answers.flatMap((obj) => obj.id);
    var object = {};
    for (var i = 0; i < countOfAnswers; ++i) {
      object[objectId[i]] = false;
    }
    return object;
  };

  const checkAnswer = (evt) => {
    evt.preventDefault();

    let answerCorrect = answers.map((obj) => obj.is_right);
    let mapAnswerCorrect = { ...answerCorrect };

    function arrayEquals(answerArray1, answerArray2) {
      return (
        Array.isArray(answerArray1) &&
        Array.isArray(answerArray2) &&
        answerArray1.length === answerArray2.length &&
        answerArray1.every((val, index) => val === answerArray2[index])
      );
    }

    let answerArray1 = Object.values(mapAnswerCorrect);
    let answerArray2 = Object.values(answer);
    if (arrayEquals(answerArray1, answerArray2)) {
        setAnswerCheck(true);
    } else {
        setAnswerCheck(false);
    }
  };

  function refreshPage() {
    window.location.reload(false);
  }

  function Result() {
    if (answerCheck === true) {
      return (
        <Alert severity="success">
          <AlertTitle>Corrent Answer</AlertTitle>
          <Link href="#"  onClick={refreshPage}>
            {"Next"}
          </Link>
        </Alert>
      );
    } else if (answerCheck === false) {
      return (
        <Alert severity="error">
          <AlertTitle>Wrong Answer</AlertTitle>
          Try again!
        </Alert>
      );
    } else {
      return <></>;
    }
  }

  return (
    <>
    
      <Container  maxWidth="xs">
        <div>

          {dataState.data.map(({ title, answer}, index) => (
            <div key={index}>
              <Typography  variant="h5">
                {title}
              </Typography>
              {answer.map(({ answer_text, id}) => (
                <div>
                  <FormControlLabel
                    control={
                      <Checkbox
                        value={id}  
                        onChange={handleSelection}
                      />
                    }
                    label={answer_text}
                   
                  />
                </div>  



              ))}
              <Button
                type="submit"
                fullWidth
                variant="contained"
                onClick={checkAnswer}
              >
                Submit 
              </Button>
              <Result />
            </div>
          ))}
        </div>
      </Container>
    </>
  );
};

export default RandomQuestions;

import React from "react";
import ConnectApi from "../ConnectApi";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";



export const GameSelector = () => {
  const API_URL = "http://127.0.0.1:8000/quiz/";
  const [dataState] = ConnectApi(API_URL);

  return (
    <>
      <div>
        <Button
          href="http://127.0.0.1:8000"
        >
          Administration
        </Button>
        <Typography
          variant="h2"
          align="center"
        >
          Games
        </Typography>
      </div>
      <div>
        <Button
          fullWidth
          variant="h2"
          href="http://localhost:3000/r/django"
        >
          <br />
          Start Game
        </Button>

      </div>
    </>
  );
};

export default GameSelector;

from django.contrib import admin
from . import models





class AnswerInlineModel(admin.TabularInline):
    model = models.Answer
    fields = [
        'answer_text', 
        'is_right'
        ]


@admin.register(models.Question)

class QuestionAdmin(admin.ModelAdmin):
    fields = [
        'title',
        'difficulty',
        'points'
        ]
    list_display = [
        'title', 
        'difficulty',
        'points'
        ]
    inlines = [
        AnswerInlineModel, 
        ] 


@admin.register(models.Answer)

class AnswerAdmin(admin.ModelAdmin):
    list_display = [
        'answer_text', 
        'is_right', 
        'question'
        ]

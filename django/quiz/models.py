from django.db import models


class Quizzes(models.Model):

    class Meta:
        verbose_name = "Quiz"
        verbose_name_plural = "Quizzes"
        ordering = ['id']

    title = models.CharField(
        max_length=255, default="New Quiz", verbose_name="Quiz Title")

    def __str__(self):
        return self.title


class UpdatedQuestion(models.Model):

    date_updated = models.DateTimeField(
        verbose_name="Last Updated", auto_now=True)

    class Meta:
        abstract = True


class Question(UpdatedQuestion):

    class Meta:
        verbose_name = "Question"
        verbose_name_plural = "Questions"
        ordering = ['id']

    SCALE = (
        (0, 'Easy'),
        (1, 'Normal'),
        (2, 'Hard'),
        (3, 'Advanced'),
        (4, 'Expert')
    )

    TYPE = (
        (0, 'Multiple Choice'),
    )

    POINTS = (
        (0, 3),
        (1, 6),
        (2, 9),
        (3, 15),
        (4, 20),
    )
    quiz = models.ForeignKey(
        Quizzes, related_name='question', on_delete=models.DO_NOTHING)
    title = models.CharField(max_length=255, verbose_name="Question title")
    difficulty = models.IntegerField(
        choices=SCALE, default=0, verbose_name="Difficulty")
    points = models.IntegerField(
        default=0, choices=POINTS, verbose_name="Points")
    is_active = models.BooleanField(
        default=False, verbose_name="Active Status")

    def __str__(self):
        return self.title


class Answer(UpdatedQuestion):

    class Meta:
        verbose_name = "Answer"
        verbose_name_plural = "Answers"
        ordering = ['id']

    question = models.ForeignKey(
        Question, related_name='answer', on_delete=models.DO_NOTHING)
    answer_text = models.CharField(
        max_length=255, verbose_name="Answer Text")
    is_right = models.BooleanField(default=False)

    def __str__(self):
        return self.answer_text
